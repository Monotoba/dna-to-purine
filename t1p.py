#
# dna purine script
# 
# 1. Scan directory for *.fasta file
# 2. Save all *.fasta files for later use
# 3. Create t1p - folder to save processed file to.
# 4. create output file using originalfile name with a p_ prefix.
# -  if a file of this name already exists it should be over written without warning.
# 5. Convert C -> Y, T -> Y, A -> R, G -> R.
# 6. Upon completion the program should end with a message: 
# -  "23 files have been converted to the purine code" 
# -  to be dismissed with OK or Escape.
#

import os
from pathlib import Path


# Gets a list of files to process
# from the current working directory
def get_fasta_files():
    p = Path('.')
    files = list(p.glob('*.fasta'))
    return files
    

def convert_to_purine(ln):
    res_ln = ""
    for ch in ln:
        if ch != '\n' and ch != '\r':
            if ch == 'A' or ch == 'G':
                res_ln = res_ln + 'R'
            elif ch == 'C' or ch == 'T':
                res_ln = res_ln + 'Y'
            else:
                # do we need to throw and error here?
                pass
    #print(res_ln)
    return res_ln        
        

def translate_dna_to_purine(filename):
    dst_dir = Path('./t1p')
    dst_dir.mkdir(parents=True, exist_ok=True)
    #print(str(dst_dir))
    dst_name = dst_dir.joinpath("p_" + str(filename))

    with filename.open('r') as src:
        # first line is header
        header = src.readline()
        with dst_name.open('w') as dst:
            # write header to destination file
            dst.write(header)
            src_ln = "_"
            while src_ln:
                # read a line from src 
                src_ln = src.readline()
                # convert dna to purine
                dst_ln = convert_to_purine(src_ln)
                # read a line from src 
                dst.write(dst_ln)

    

def main():
    print("Scanning directory...")
    files = get_fasta_files()
    # process files
    file_count = 0
    for f in files:
        print("Processing file: " + str(f))
        translate_dna_to_purine(f)
        file_count += 1
    print(str(file_count) + " files have been converted to the purine code")
    input("Ok to Quit? Y/n")
    return

if __name__ == "__main__":
    main()

